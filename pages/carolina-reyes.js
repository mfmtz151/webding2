import styles from '../styles/carolina-reyes/Section1.module.css';
import Section1 from '../src/components/Section1';
import Section2 from '../src/components/Secton2';
import Section4 from '../src/components/Secton4';

export default function Home() {
  return (
    <div>
        <Section1></Section1>
        <Section2></Section2>
        <Section4></Section4>
    </div>
  )
}
